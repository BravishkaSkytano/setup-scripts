#!/bin/bash

read -rp "Install Nix package manager?. Yes(y) / No(n) / Cancel(c):- " choice
if [ "$choice" = "y" ]; then
	echo "Installing Nix..."
	sh <(curl -L https://nixos.org/nix/install)
	. ~/.nix-profile/etc/profile.d/nix.sh
elif [ "$choice" = "n" ];then
	echo "Skipping..."
elif [ "$choice" = "c" ];then
	echo "Aborting"
	exit
fi

echo "To update Nix run 'nix-channel --update; nix-env -iA nixpkgs.nix nixpkgs.cacert'"
echo "You can also just run 'nix-channel --update'"

sleep 5

# install packages
echo "Installing Nix packages..."

sleep 5

nix-env -iA \
    nixpkgs.macchina \
    nixpkgs.micro \
    nixpkgs.starship

read -rp "Install fzf (y/n):- " choice
case $choice in
    [yY]* ) nix-env -iA nixpkgs.fzf ;;
    [nN]* ) echo "Skipping..." ;;
esac

echo "Done installing packages."
echo "Setup successful."
echo "Have fun with your new system!"
